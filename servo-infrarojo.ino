/*
Codigo que utiliza un control infrarojo mediante la codificacion nec para controlar los movimientos de uno o varios servomotores 

*/
#include <IRremote.h>
 
#include <Servo.h> //Importamos la librería para controlar servomotores
 
Servo servomotor1; //Nombre del servomotor
Servo servomotor2;
 
int RECV_PIN = 11; //Pin del receptor IR
IRrecv irrecv(RECV_PIN);
 
decode_results results; //Variable del código NEC
 
int posicion1=90; //Variable para determinar la posición angular del servo
int posicion2=90;
int servo1;
void setup()
{
 
  irrecv.enableIRIn(); //Inicializamos el receptor IR
  servomotor1.attach(3); //Asociamos el servomotor al pin 3 del Arduino
  servomotor2.attach(7);
 
}
 
void loop()
{
if (irrecv.decode(&results)) //Si el receptor IR recibe una señal, entra dentro de la función
 irrecv.resume();
  {
if (results.value == 0b111111110110001010011101)
    {                
        posicion1= posicion1 + 90;
        if (posicion1 >= 180)
            {
            posicion1=160;
            }
            
    }
else if(results.value == 0b111111111110001000011101)
    {
        posicion1= posicion1 - 90;
        if(posicion1 <= 0)
            {
            posicion1=10;
            }     
     }
     
if(results.value == 0b111111111110000000011111)
    {
        posicion2= posicion2 + 90; 
        if (posicion1 >= 180)
            {
            posicion1=160;
            }       
     }
else if(results.value == 0b111111111110000000011111)
    {
        posicion2= posicion2 - 90; 
        if(posicion1 <= 0)
            {
            posicion1=10;
            }       
     }     
     
servomotor1.write(posicion1); //El servo se posiciona a la posición angular que determina la variable "posicion"
servomotor2.write(posicion2);
    
    }
}
